//
//  Workman.swift
//  StructurAndClassTest
//
//  Created by Aleksey Knysh on 1/26/22.
//

import Foundation

struct Workman {
    var name: String
    var surname: String
    var presenceAtWork: Bool
}
