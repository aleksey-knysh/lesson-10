//
//  ViewController.swift
//  StructurAndClassTest
//
//  Created by Aleksey Knysh on 1/17/22.
//

import UIKit

class ViewController: UIViewController {

    let firstWorker = Worker(name: "Tima", surname: "Danilyuk", presenceAtWork: Bool.random())
    let secondWorker = Worker(name: "Artur", surname: "Danilyuk", presenceAtWork: Bool.random())
    let thirdWorker = Worker(name: "Sasha", surname: "Petrov", presenceAtWork: Bool.random())
    let fourthWorker = Worker(name: "Vasia", surname: "Sidorov", presenceAtWork: Bool.random())
    let fifthWorker = Worker(name: "Roma", surname: "Latynia", presenceAtWork: Bool.random())

    var arrayOfWorkingWorkers = [Worker]()
    var arrayOfMissingWorkers = [Worker]()
    var arrayWorkerSorted = [Worker]()

    let firstWorkman = Workman(name: "Tima", surname: "Danilyuk", presenceAtWork: Bool.random())
    let secondWorkman = Workman(name: "Artur", surname: "Danilyuk", presenceAtWork: Bool.random())
    let thirdWorkman = Workman(name: "Sasha", surname: "Petrov", presenceAtWork: Bool.random())
    let fourthWorkman = Workman(name: "Vasia", surname: "Sidorov", presenceAtWork: Bool.random())
    let fifthWorkman = Workman(name: "Roma", surname: "Latynia", presenceAtWork: Bool.random())

    var arrayOfWorkingWorkmans = [Workman]()
    var arrayOfMissingWorkmans = [Workman]()
    var arrayWorkmanSorted = [Workman]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let arrayWorker = [firstWorker, secondWorker, thirdWorker, fourthWorker, fifthWorker]

        arrayWorker.forEach {
            if $0.presenceAtWork == true {
                print("\($0.name) \($0.surname) - работает")
                arrayOfWorkingWorkers.append($0)
            } else {
                $0.presenceAtWork = true
                print("\($0.name) \($0.surname) - вызвали на работу")
                arrayOfMissingWorkers.append($0)
            }
        }

        arrayWorkerSorted = arrayOfWorkingWorkers.sorted(by: { $0.surname < $1.surname }) + arrayOfMissingWorkers.sorted(by: { $0.name < $1.name })
        arrayWorkerSorted.forEach { print("\n\($0.name) \($0.surname)") }

        let newArray = arrayWorker
        newArray[0].name = "Ar"
        newArray[0].surname = "dfh"
        newArray[1].name = "sdg"
        newArray[1].surname = "sdhnf"
        newArray[2].name = "xcfh"
        newArray[2].surname = "adfhk"
        newArray[3].name = "xdzhkll"
        newArray[3].surname = "sdgzxdkgul"
        newArray[4].name = "xdszj;mnu"
        newArray[4].surname = "tuoij"

        arrayWorker.forEach { print("Старый массив \($0.name) \($0.surname)") }
        newArray.forEach { print("Новый массив \($0.name) \($0.surname)") }

        // MARK: - struct Workman

        let arrayWorkman = [firstWorkman, secondWorkman, thirdWorkman, fourthWorkman, fifthWorkman]

        arrayWorkman.forEach {
            if $0.presenceAtWork == true {
                print("\($0.name) \($0.surname) - работает")
                arrayOfWorkingWorkmans.append($0)
            } else {
                var work = $0
                work.presenceAtWork = true
                print("\($0.name) \($0.surname) - вызвали на работу")
                arrayOfMissingWorkmans.append($0)
            }
        }

        arrayWorkmanSorted = arrayOfWorkingWorkmans.sorted(by: { $0.surname < $1.surname }) + arrayOfMissingWorkmans.sorted(by: { $0.name < $1.name })
        arrayWorkmanSorted.forEach { print("\n\($0.name) \($0.surname)") }

        var newArrayWorkmanStruct = arrayWorkman
        newArrayWorkmanStruct[0].name = "Ar"
        newArrayWorkmanStruct[0].surname = "dfh"
        newArrayWorkmanStruct[1].name = "sdg"
        newArrayWorkmanStruct[1].surname = "sdhnf"
        newArrayWorkmanStruct[2].name = "xcfh"
        newArrayWorkmanStruct[2].surname = "adfhk"
        newArrayWorkmanStruct[3].name = "xdzhkll"
        newArrayWorkmanStruct[3].surname = "sdgzxdkgul"
        newArrayWorkmanStruct[4].name = "xdszj;mnu"
        newArrayWorkmanStruct[4].surname = "tuoij"

        arrayWorkman.forEach { print("Старый массив структуры \($0.name) \($0.surname)") }
        newArrayWorkmanStruct.forEach { print("Новый массив структуры \($0.name) \($0.surname)") }
    }
}
