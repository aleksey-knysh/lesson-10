//
//  ClassWorker.swift
//  StructurAndClassTest
//
//  Created by Aleksey Knysh on 1/17/22.
//

import Foundation

class Worker {
    var name: String
    var surname: String
    var presenceAtWork: Bool

    init(name: String, surname: String, presenceAtWork: Bool) {
        self .name = name
        self .surname = surname
        self .presenceAtWork = presenceAtWork
    }
}
